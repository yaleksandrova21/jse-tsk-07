package ru.yaleksandrova.tm;

import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.model.Command;
import ru.yaleksandrova.tm.repository.CommandRepository;
import ru.yaleksandrova.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseArg(final String arg) {
        switch(arg) {
            case ArgumentConst.ABOUT: showAbout(); break;
            case ArgumentConst.VERSION: showVersion(); break;
            case ArgumentConst.HELP: showHelp(); break;
            case ArgumentConst.INFO: showInfo(); break;
            default: showErrorArgument();
        }
    }

    public static void parseCommand(final String command) {
        switch(command) {
            case ApplicationConst.ABOUT: showAbout(); break;
            case ApplicationConst.VERSION: showVersion(); break;
            case ApplicationConst.HELP: showHelp(); break;
            case ApplicationConst.INFO: showInfo(); break;
            case ApplicationConst.EXIT: exitApplication(); break;
            default: showErrorCommand();
        }
    }

    private static void showErrorCommand() {
        System.err.println("Error! Command not found...");
    }

    private static void showErrorArgument() {
        System.err.println("Error! Argument not supported...");
        System.exit(1);
    }

    public static void  parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
    }

    public static void exitApplication() {
        System.exit( 0 );
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yulia Aleksandrova");
        System.out.println("E-mail: yaleksanrova@yandex.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }


    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
        }
    }


