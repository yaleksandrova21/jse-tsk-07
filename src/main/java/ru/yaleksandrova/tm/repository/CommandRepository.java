package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(
            ApplicationConst.ABOUT, ArgumentConst.ABOUT, "Display developer info..."
    );
    public static final Command HELP = new Command(
            ApplicationConst.HELP, ArgumentConst.HELP, "Display list of commands..."
    );
    public static final Command VERSION = new Command(
            ApplicationConst.VERSION, ArgumentConst.VERSION, "Display program version..."
    );;
    public static final Command INFO = new Command(
            ApplicationConst.INFO, ArgumentConst.INFO, "Display system information..."
    );;
    public static final Command EXIT = new Command(
            ApplicationConst.EXIT, null, "Close application..."
    );;

    public static final Command[] COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }
}
